package aramos.network.excercises;



import java.net.*;
import java.io.*;

public class RemoteShellCommandServer {
    public static void main(String[] args) throws IOException, InterruptedException {
        
        if (args.length != 1) {
            System.err.println("Usage: java RemoteShellServer <port number>");
            System.exit(1);
        }
        
        int portNumber = Integer.parseInt(args[0]);
        
        try (
            ServerSocket serverSocket =
                new ServerSocket(portNumber);
            Socket clientSocket = serverSocket.accept();     
            PrintWriter socketout =
                new PrintWriter(clientSocket.getOutputStream(), true);                   
            BufferedReader socketin = new BufferedReader(
                new InputStreamReader(clientSocket.getInputStream()));
        ) {
            String inputLine;
            while ((inputLine = socketin.readLine()) != null) {
                executeCommand(inputLine,socketout);
            }
        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port "
                + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        }
    }

	private static void executeCommand(String inputLine, PrintWriter socketout) throws IOException, InterruptedException {
		System.out.println("What command would you like to execute?");
		// Read line from user
		BufferedReader stdIn = new BufferedReader(new InputStreamReader(
				System.in));

		System.out.println("Executing command: " + inputLine);

		//Execute in shell
		Process p = Runtime.getRuntime().exec(inputLine);
		// Wait for the process to terminate
		p.waitFor();
		
		//Read the output of the process and send it to our standard output 
		BufferedReader reader = 
		         new BufferedReader(new InputStreamReader(p.getInputStream()));

		String line = "";
		while ((line = reader.readLine())!= null) {
		    	socketout.println(line);
		}
		
	}
}

