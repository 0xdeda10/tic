/**
 * 
 */
package aramos.basics.show;

/**
 * @author alvaro
 *
 */
public class MiPrograma {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int[] array = {2,4,6,5,4,33,56,1,2346,45,65,56};
		
		IntArrayUtils.directPrint(array);
		
	}

}
