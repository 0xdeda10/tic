package aramos.basics.show;

import java.util.Scanner;

public class Adivina {
	static String line = "";
	static final int NUM_ITERACIONES = 6;
	static int numerosecreto = getRandom();
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		scanner.useDelimiter(System.getProperty("line.separator"));
		while (!line.equals("quit")) {
			numerosecreto = getRandom();
			System.out
					.println("¡Intenta adivinar el número del 1 al 5! Escríbelo y pulsa ENTER:");
			System.out.println("[Si quieres salir escribe \"quit\"]");
	
			for(int i =0; i<NUM_ITERACIONES; i++){
				line = scanner.next();
				if (line.equals("quit")){
					break;
				} else if (Integer.valueOf(line)==numerosecreto){
					System.out.println("¡Has acertado! ¿Quieres jugar otra vez?");
					line = scanner.next();
					if (line.compareToIgnoreCase("y")==0) {
						i=-1;
						// Esto es código repetido...
						numerosecreto = getRandom();
						System.out
						.println("¡Intenta adivinar el número del 1 al 5! Escríbelo y pulsa ENTER:");
				System.out.println("[Si quieres salir escribe \"quit\"]");
						continue;
					} else {
						line="quit";
						break;
					}
				} else {
					System.out.println("Nope... tienes "+ (NUM_ITERACIONES-i-1) +" intentos más.");
				}
			}
			if (!line.equals("quit")){
				System.out.println("¡Has fallado!¡Empezamos otra vez!");
				
			}
			
		}
		
		System.out.println("¡Hasta pronto!");
		scanner.close();
	}

	
	public static int getRandom() {
		return (int) Math.round(5*Math.random()+1);
	}
}
