package aramos.basics.show;

import java.util.Scanner;

public class Eco {
	static String line = "";
	static final int NUM_ITERACIONES = 6;
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		scanner.useDelimiter(System.getProperty("line.separator"));
		
		
		
//		// Bucle infinito con respuesta condicional
//		while(true){
//			line = scanner.next();
//			if (line.equals("tonto")){
//				System.out.println("eso me ha dolido");
//			} else {
//				System.out.println(line);
//			}
//		}
//		
		
//		// Bucle con salida
//		System.out.println("[Si quieres salir escribe \"quit\"]");
//		do {
//			line = scanner.next();
//			System.out.println(line);
//
//		} while(!line.equals("quit"));
//		
		
		// Bucle con número de iteraciones limitado y break
		System.out.println("[Si quieres salir escribe \"quit\"]");
		for(int i=0;i<3; i++){
			line = scanner.next();
			if (line.equals("quit")){
				break;
			} else {
				System.out.println(line);
			}
		}
		
//		for(int i=0;(i<NUM_ITERACIONES)&& !line.equals("quit"); i++){
//			line = scanner.next();
//			System.out.println(line);
//		}
		
		System.out.println("¡Hasta pronto!");
		scanner.close();
	}

}
