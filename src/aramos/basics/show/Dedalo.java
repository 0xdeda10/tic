package aramos.basics.show;

import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Dedalo {
	static Date dt;
	static String line = "";
	static char [][] dedalo;
	static int x,y,xold,yold,x_Mino,y_Mino;
	static int turno=0;
	static int TURNO_MINO=4;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		dedalo=rellenaDedalo();
		x=1;
		y=1;
		Scanner scanner = new Scanner(System.in);
		scanner.useDelimiter(System.getProperty("line.separator"));
		int k;
		dt=Calendar.getInstance().getTime();
		while (!line.equals("quit")) {
			k=turno-TURNO_MINO;
			actualizaDedalo();
			printDedalo();
			xold=x;
			yold=y;
			line= scanner.next();
			switch (line) {
				case "w": x--;
						break;
				case "s": x++;
						break;
				case "a": y--;
						break;
				case "d": y++;
			}
			
			if (Character.compare(dedalo[x][y],'X')==0){
				actualizaDedalo();
				printDedalo();
				System.out.println("¡¡¡A SALVO!!!");
				break;
			} else if (Character.compare(dedalo[x][y],' ')!=0) {
				System.out.println("OUCH!!!");
				x=xold;
				y=yold;
			} else if ((x==x_Mino) && (y==y_Mino)) {
				muerte();
				break;
			}
			if (k>=0){
				System.out.println("¡¡¡EL MINOTAURO SE MUEVE!!!");
				x_Mino = x_Mino_turnos[k];
				y_Mino = y_Mino_turnos[k];
				if ((x==x_Mino) && (y==y_Mino)) {
					muerte();
					break;
				}
			}
			turno++;
		}
System.out.println(">>>>>>>>    "+ (Calendar.getInstance().getTime().getTime()- dt.getTime())/1000 +" secs    <<<<<<<<<<");
		scanner.close();
	}
	
	private static void muerte() {
		actualizaDedalo();
		printDedalo();
		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("¡¡AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAARRRGH!!");
		System.out.println("");
		System.out.println("                  00000                    ");
		System.out.println("               000000000000                 ");
		System.out.println("            0000   000   0000  ");
		System.out.println("             000   0 0   000   ");
		System.out.println("              00000   00000    ");
		System.out.println("                000000000      ");
		System.out.println("                \\+++++++/       ");
		System.out.println("                 \\+++++/       ");
		System.out.println("                   000       ");
		System.out.println("");
		System.out.println("               DEATH!!!!!");
	}
	
	private static void printDedalo() {
		System.out.println("Turno: "+turno);
		for(int i=0; i<10;i++){
			System.out.println(dedalo[i]);
		}
		
	}
	private static void actualizaDedalo() {
		
		dedalo=rellenaDedalo();
		dedalo[x][y]='*';
		if (x_Mino>0){
			dedalo[x_Mino][y_Mino]='0';
		}
	}
	
	private static char[][] rellenaDedalo() {
		char [][] d = new char[10][10];
		d [0] = "___________".toCharArray();
		d [1] = "| |   |   |".toCharArray();
		d [2] = "| | | | | |".toCharArray();
		d [3] = "|   | | | |".toCharArray();
		d [4] = "|-|-| | | |".toCharArray();
		d [5] = "|     | | |".toCharArray();
		d [6] = "| |---| | |".toCharArray();
		d [7] = "| |   | | |".toCharArray();
		d [8] = "|   |   | |".toCharArray();
		d [9] = "--------|X|".toCharArray();
		return d;
	// 	
	}
	static int x_Mino_turnos[]={1,2,3,3,3,2,1,1,1,2,3,4,5,5,5,5,5,6,7,8,8,8,7,7,7,8,8,8,7,6,5,4,3,2,1,1,1,2,3,4,5,6,7,8,9};
	static int y_Mino_turnos[]={1,1,1,2,3,3,3,4,5,5,5,5,5,4,3,2,1,1,1,1,2,3,3,4,5,5,6,7,7,7,7,7,7,7,7,8,9,9,9,9,9,9,9,9,9};
	

}
