package aramos.basics.show;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ExecuteInShell {
	

	public static void main(String[] args) throws IOException, InterruptedException {

		System.out.println("What command would you like to execute?");
		// Read line from user
		BufferedReader stdIn = new BufferedReader(new InputStreamReader(
				System.in));
		String userInput = stdIn.readLine();

		System.out.println("Executing command: " + userInput);

		//Execute in shell
		Process p = Runtime.getRuntime().exec(userInput);
		// Wait for the process to terminate
		p.waitFor();
		
		//Read the output of the process and send it to our standard output 
		BufferedReader reader = 
		         new BufferedReader(new InputStreamReader(p.getInputStream()));

		String line = "";
		while ((line = reader.readLine())!= null) {
		    	System.out.println(line);
		}
		

	}
}
