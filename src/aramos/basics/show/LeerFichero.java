/**
 * 
 */
package aramos.basics.show;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 * @author alvaro
 * 
 */
public class LeerFichero {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Leer el path que escriba el usuario

		System.out
				.println("Escribe la ruta (path) del fichero que desea imprimir:");
		Scanner sc = new Scanner(System.in);
		sc.useDelimiter(System.getProperty("line.separator"));
		String inputfile = sc.next();
		// inputfile contiene el path al fichero
		sc.close();
		
		BufferedReader inreader;
		
		try {
			inreader = new BufferedReader(new FileReader(inputfile));
		} catch (FileNotFoundException e) {
			// El fichero no existe: informamos y salimos
			System.out.println("Mal asunto: no existe el fichero...");
			return;
		}
		// El fichero existe
		try {
			
			/* el método ready() de BufferedReader nos indica si hay más datos por leer 
			* o hemos llegado al final del fichero.
			*/
			while (inreader.ready()) {
				String linea;
				
				// Leemos una línea
				linea = inreader.readLine();
				
				// La imprimimos
				System.out.println(linea);
			}
			inreader.close();
		} catch (IOException e) {
			System.out.println("Final inesperado de lectura");
			return;
		}

	}

}
