package aramos.basics.show;

public class Scope {

	//Declaramos una variable miembro
	int variableMiembro;
	
	public void metodo1() {
		// Declaramos e inicializamos a 0 una variable de tipo int
		int unaVariable = 0;
		
		if (unaVariable<3){
			// Declaramos e inicializamos a 0 otra variable de tipo int
			// dentro del bloque if
			int otraVariable;
			
			//Aquí las dos variables son visibles
			otraVariable=unaVariable+2;
		}
		
		// Aquí, fuera del bloque if, otraVariable ya no es visible pero unaVariable sí.
		
		//La variable miembro es accesible
		unaVariable=variableMiembro;
	}
	
	public void metodo2() {
		//La variable miembro es accesible pero unaVariable no
		variableMiembro =variableMiembro/3;
	}
}
