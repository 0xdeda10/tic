package aramos.basics.show;

public class ImprimeInversoYMinimoArray {

	public static void main(String[] args) {

		int[] unArray = { 12, -12, 1, -1, 2 };

		
		// Imprimimos un array a la inversa
		imprimeArrayInverso(unArray);

		
		
		// valor mínimo del array
		int min = minimo(unArray);

		// TODO Imprimimos min
		System.out.println("--- Vamos a hallar el mínimo ---");
		System.out.println(min);

		
		
		//TODO Haz un método que calcule la suma de los elementos del array e imprímela
		int suma = suma(unArray);
		System.out.println("--- Vamos a hallar la suma ---");
		System.out.println("La suma es : " + suma);
		
	}
	

	private static int suma(int[] unArray) {
		
		int suma =0;
		
		for(int k = 0; k < unArray.length; k++){
			suma = suma + unArray[k];
		}
		
		
		return suma;
	}


	public static int minimo(int[] unArray) {
		
		//TODO Implementa el método
		/*
		 * Para encontrarlo lo mejor es recorrer el array y comparar cada
		 * elemento con el valor mínimo hasta el momento: - si el elemento es
		 * menor, este será el nuevo mínimo y pasaremos al siguiente elemento
		 * del array; - si el elemento es mayor, pasaremos directamente al
		 * siguiente elemento.
		 * 
		 * El mínimo lo hemos inicializado al valor máximo de cualquier int
		 * (Integer.MAX_VALUE) para que el primer elemento del array sea menor o
		 * igual que él
		 */
		int minimo = Integer.MAX_VALUE;

		for(int k = 0; k < unArray.length; k++){
			if ( unArray[k] < minimo ) {
				minimo = unArray[k];
			}
		}
		return minimo;
	
		
		//return minimo;
	}

	public static void imprimeArrayInverso(int[] unArray) {
		
		/*
		 * Haz un bucle que recorra el array (unArray) desde el último elemento
		 * hasta el primero imprimiendo cada elemento en una línea.
		 */
		
		for(int k = unArray.length-1; k>=0 ; k--){
			System.out.println( unArray[k] );
		}
	}

}
