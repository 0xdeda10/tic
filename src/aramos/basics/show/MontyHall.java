package aramos.basics.show;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class MontyHall {
	static Scanner s;
	static Writer fw;
	static int prize; 
	static int userbox;
	static int montybox;
	static int openbox;
	static boolean swapped;
	static int win;
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		fw =  new BufferedWriter(new OutputStreamWriter(
	              new FileOutputStream("resultadosmonty"+(new Random()).nextLong()+".csv")));
		s = new Scanner(System.in);
		fw.write("GANA,CAMBIA,PREMIO,MONTY\n");
		while(oneContest()){};

	}

	

	private static boolean oneContest() throws IOException {
		prize = hidePrize();
		clear();
		printBanner();
		userbox=0;
		montybox=0;
		openbox=0;
		swapped = false;
		String input;
		while(true){
			input = s.nextLine();
			if(input.length()>1){
				printerr1();
				continue;
			}else {
				if (input.startsWith("q")) return false;
				try {userbox=Integer.parseInt(input);}
				catch (NumberFormatException e){
					printerr1();
					continue;
				}
				if ((userbox<1)||(userbox>3)){
					printerr1();
					continue;
				} else break;
			}
		}
		montybox= montyOpenBox(userbox);
		int aux;
		while(true){
			input = s.nextLine();
			if(input.length()>1){
				printerr2();
				continue;
			}else {
				if (input.startsWith("q"))return false;
				else if (input.startsWith("c")){
					aux=userbox;
					userbox=montybox;
					montybox=aux;
					swapped=true;
					break;
				}else if (input.startsWith("m")){
					break;
				}else{
					printerr2();
					continue;
				}
			}
		}
		printResult();
		s.nextLine();
		clear();
		return true;
	}


	private static void clear(){
		for(int i=0; i<100;i++) System.out.println();
	}
	private static void printResult() throws IOException {
		clear();
		String text;
		if (userbox==prize) {
			text = "*** ¡¡¡¡Has ganado!!!! ***";
			win=1;
		}
		else {
			text = "Has perdido...";
			win=0;
		}
		System.out.println(text);
		System.out.println();
		System.out.println("	-----	-----	-----");
		String boxes = "	| 1 |	| 2 |	| 3 |";
		boxes = boxes.replace(String.valueOf(prize), "*");
		boxes = boxes.replace(String.valueOf(userbox), "U");
		boxes = boxes.replace(String.valueOf(openbox), "X"); //smile \u263A
		boxes = boxes.replace(String.valueOf(montybox), "M");
		
		System.out.println(boxes);
		System.out.println("	-----	-----	-----");
		System.out.println();
		System.out.println( "Pulsa ENTER para otro concursante.");
		
		//file
		
		int s=(swapped)?1:0;
		fw.write(win+","+s+","+prize+","+montybox+"\n");
		fw.flush();
		
	}



	private static void printBanner() {
		System.out.println("El premio está en una de estas cajas... elige una.");
		System.out.println();
		System.out.println("	-----	-----	-----");
		System.out.println("	| 1 |	| 2 |	| 3 |");
		System.out.println("	-----	-----	-----");
		System.out.println();
		printerr1();
		
	}



	private static int hidePrize() {
		Random r = new Random();
		return r.nextInt(3)+1;
	}

	private static int montyOpenBox(int box) {
		Random r = new Random();
		List<Integer> numbers=new ArrayList<Integer>(2);
		for(int i=1;i<4;i++){
			if ((i!=box)&&(i!=prize)) numbers.add(i);
		}
		if (numbers.size()==1) {
			openbox=numbers.get(0);
			printOpenBox();
			return prize;
		}
		if (r.nextBoolean()){
			openbox=numbers.get(1);
			printOpenBox();
			return numbers.get(0);
		}
		else {
			openbox=numbers.get(0);
			printOpenBox();
			return numbers.get(1);
		}
	}

	private static void printOpenBox() {
		clear();
		System.out.println("Has elegido la caja "+userbox+ " y Monty ha abierto la caja "+ openbox);
		System.out.println();
		System.out.println("	-----	-----	-----");
		String boxes = "	| 1 |	| 2 |	| 3 |";
		boxes = boxes.replace(String.valueOf(userbox), "U");
		boxes = boxes.replace(String.valueOf(openbox), "X"); //smile \u263A angry \u2639
		boxes = boxes.replace(String.valueOf(montybox), "M");
		System.out.println(boxes);
		System.out.println("	-----	-----	-----");
		System.out.println();
		printerr2();
		
	}

	private static void printerr2() {
		System.out.println("Escribe (c) para Cambiar, (m) para Mantener (q para salir)");
	}
	private static void printerr1() {
		System.out.println("Escribe 1, 2 o 3 (q para salir)");
		
	}
	public void finalize(){
		s.close();
		try {
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
