package aramos.basics.exercises;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Fibonacci {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int x ;
		Scanner scanner = new Scanner(System.in);
		scanner.useDelimiter(System.getProperty("line.separator"));
		System.out.println("Escribe un número y calcularé la sucesión de Fibonacci hasta ese término... (no te pases del "+ Integer.MAX_VALUE +")");
		x= scanner.nextInt();
		System.out.println("Mmm... ok... me pongo con ello...");
		System.out.println("------------------------------------");
		System.out.println(">>>>       MÉTODO ITERATIVO     <<<<");
		System.out.println("------------------------------------");
		int[] resultado=fiboIter(x);
		System.out.println(Arrays.toString(resultado));
		
		
		System.out.println("------------------------------------");
		System.out.println(">>>>       MÉTODO RECURSIVO     <<<<");
		System.out.println("------------------------------------");
		ArrayList<Integer> resultado2 =fiboRecur(x);
		System.out.println(resultado2.toString());
		scanner.close();

	}

	private static ArrayList<Integer> fiboRecur(int x) {
		ArrayList<Integer> fibo = new ArrayList<Integer>();
		fibo.add(1);
		fibo.add(1);
		return fiboRecurImpl(fibo,x);
	}

	private static ArrayList<Integer> fiboRecurImpl(ArrayList<Integer> fibo,int n) {
		int l = fibo.size();
		
		if (n==l) return fibo;
		
		fibo.add(fibo.get(l-1)+fibo.get(l-2));
		
		return fiboRecurImpl(fibo,n);
		
	}

	private static int[] fiboIter(int x) {
		int []fibo = new int[x];
		fibo[0]=1;
		fibo[1]=1;
		for(int i = 2; i<x; i++){
			fibo[i]=fibo[i-1]+fibo[i-2];
		}
		return fibo;
	}

}
