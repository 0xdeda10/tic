package aramos.basics.exercises;

import java.util.Scanner;

public class Palindrome {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		long x ;
		Scanner scanner = new Scanner(System.in);
		scanner.useDelimiter(System.getProperty("line.separator"));
		System.out.println("Escribe un número y te diré si es palíndromo... (no te pases del "+ Long.MAX_VALUE +")");
		x= scanner.nextLong();
		System.out.println("Mmm... ok... me pongo con ello...");
		System.out.println("------------------------------------");
		System.out.println(">>>>       MÉTODO NUMÉRICO      <<<<");
		System.out.println("------------------------------------");
		if (Integers.palindromeNum(x)) 
			System.out.println(">>>>  ¡¡¡ "+x +" es palíndromo !!!  <<<<");
		else 
			System.out.println(">>>>  ¡¡¡ "+x +" NO es palíndromo !!!  <<<<");
		System.out.println("------------------------------------");
		System.out.println(">>>>  MÉTODO BASADO EN CADENAS  <<<<");
		System.out.println("------------------------------------");
		if (Integers.palindromeNum(x))
			System.out.println(">>>>  ¡¡¡ "+x +" es palíndromo !!!  <<<<");
		else 
			System.out.println(">>>>  ¡¡¡ "+x +" NO es palíndromo !!!  <<<<");
		scanner.close();
	}

}
