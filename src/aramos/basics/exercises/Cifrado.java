/**
 * 
 */
package aramos.basics.exercises;

import java.util.HashMap;
import java.util.Scanner;

/**
 * @author alvaro
 * 
 */
public class Cifrado {

	private static final char[] letras = { 'a', 'b', 'c', 'd', 'e', 'f', 'g',
			'h', 'i', 'j', 'k', 'l', 'm', 'n', 'ñ', 'o', 'p', 'q', 'r', 's',
			't', 'u', 'v', 'w', 'x', 'y', 'z' };
	private static final char[] codigo = { 'q', 'r', 's', 't', 'u', 'v', 'w',
			'x', 'y', 'z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
			'k', 'l', 'm', 'n', 'ñ', 'o', 'p' };

	/**
	 * @param arg
	 */
	public static void main(String[] args) {
		
		// Creamos un HashMap
		HashMap map = new HashMap();

		for (int i = 0; i < letras.length; i++)
			map.put(letras[i], codigo[i]);

		// Creo un nuevo Scanner
		System.out.println("Escribe un texto:");
		Scanner s = new Scanner(System.in);
		s.useDelimiter(System.getProperty("line.separator"));

		// Leo una linea de la entrada del usuario
		String line = s.next();

		//Recorro line desde el primer caracter hasta el último
		String resultado = "";
		for (int i=0; i<line.length();i++){
			char caracter_i = line.charAt(i);
			if (!map.containsKey(caracter_i))
				resultado = resultado + caracter_i;
			else {	
				char codigo_i= (char) map.get(caracter_i);
				resultado = resultado + codigo_i;
			}
		}
		
		System.out.println(resultado);
		
		s.close();
	}

}
