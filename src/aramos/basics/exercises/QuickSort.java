package aramos.basics.exercises;

import java.util.Arrays;


public class QuickSort {
 
 
    public static void main(String args[]) {
        
        int[] unsorted = {32, 39,21, 45, 23, 3, 13, 34, 1,50};
        quickSort(unsorted,0,unsorted.length-1);
        System.out.println("Sorted:" +Arrays.toString(unsorted));
        
        int[] test = { 5, 3, 2, 1};
        quickSort(test,0,test.length-1);
        System.out.println("Sorted:" +Arrays.toString(test));
    }  
 
   
    public static void quickSort(int[] unsorted, int first, int last){
    	System.out.printf("Sorting array from index %d to %d %s: %n",first, last, Arrays.toString(unsorted));
    	if (first>=last) return;
        int p = HoarePartition(unsorted,first,last);
        System.out.println("Pivot: "+p);
        quickSort(unsorted,first,p);
        quickSort(unsorted,p+1,last);
        
        

    }


	private static int HoarePartition(int[] unsorted, int first, int last) {
		int temp;
		int pivot = unsorted[first];
		int i=first-1;
		int j=last+1;
		while(true){
			do
				j--;
			while (unsorted[j]>pivot);
			do 
				i++;
			while (unsorted[i]<pivot);
			if (i<j){
				temp=unsorted[j];
				unsorted[j]=unsorted[i];
				unsorted[i]=temp;
				System.out.println("Swapped "+unsorted[i]+" with "+unsorted[j]);
				System.out.println(Arrays.toString(unsorted));
			} else {
				return j;
			}
		}

	}

}
