/**
 * 
 */
package aramos.basics.exercises;

import java.util.Scanner;

/**
 * @author alvaro
 * 
 */

public class SumaImpares {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// Creo un nuevo Scanner
		System.out.println("Escribe un número:");
		Scanner s = new Scanner(System.in);
		s.useDelimiter(System.getProperty("line.separator"));

		// Leo una linea de la entrada del usuario
		String line = s.next();
		// convierto la linea en un entero y lo guardo en una variable n
		int n = Integer.parseInt(line);
		int suma = 0;

		// Recorro desde 1 hasta n todos los valores (estarán en k)
		for (int k = 1; k <= n; k++) {
			if (k % 2 != 0) {
				// k es impar
				// suma a lo que llevo sumado k
				suma = suma + k;
			}
		}
		System.out.println(suma);
		s.close();
	}

}
