package aramos.basics.exercises;

import java.util.Scanner;

public class StringReverse {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		scanner.useDelimiter(System.getProperty("line.separator"));
		System.out.println("Escribe una cadena... le daré la vuelta");
		String s= scanner.next();
		System.out.println("------------------------------------");
		System.out.println(">>>>       MÉTODO ITERATIVO     <<<<");
		System.out.println("------------------------------------");
		String resultado=reverseIter(s);
		System.out.println(resultado);
		
		
		System.out.println("------------------------------------");
		System.out.println(">>>>       MÉTODO RECURSIVO     <<<<");
		System.out.println("------------------------------------");
		resultado=reverseRecur(s);
		System.out.println(resultado);
		scanner.close();
	}

	private static String reverseRecur(String s) {
		if (s.length()==0) return s;
		String r=String.valueOf(s.charAt(0));
		return reverseRecur(s.substring(1))+r;
	}

	private static String reverseIter(String s) {
		String r="";
		int l=s.length();
		for(int i=0;i<l;i++){
			r=r+s.charAt(l-1-i);
		}
		return r;
	}

}
