package aramos.basics.exercises;

import java.util.Scanner;

public class Prime {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		long x ;
		Scanner scanner = new Scanner(System.in);
		scanner.useDelimiter(System.getProperty("line.separator"));
		System.out.println("Escribe un número y te diré si es primo... (no te pases del "+ Long.MAX_VALUE +")");
		x= scanner.nextLong();
		System.out.println("Mmm... ok... me pongo con ello...");
		long resultado = Integers.testprimo(x);
		System.out.println(">>>>     Ya está!    <<<<");
		if (resultado==0)
			System.out.println(">>>>  ¡¡¡"+x +" es primo!!!  <<<<");
		else
			System.out.println(">>>> "+x +" es divisible por "+ resultado +"  <<<<");
		scanner.close();
	}
	
	
	
}
