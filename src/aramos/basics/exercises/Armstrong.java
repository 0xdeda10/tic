package aramos.basics.exercises;

import java.util.Scanner;

public class Armstrong {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		long x ;
		Scanner scanner = new Scanner(System.in);
		scanner.useDelimiter(System.getProperty("line.separator"));
		System.out.println("Escribe un número y te diré si es un número de Armstrong... (no te pases del "+ Long.MAX_VALUE +")");
		x= scanner.nextLong();
		System.out.println("Mmm... ok... me pongo con ello...");
		System.out.println("------------------------------------");
		System.out.println(">>>>       MÉTODO JERÁRQUICO      <<<<");
		System.out.println("------------------------------------");
		if (Integers.armstrongJer(x)) 
			System.out.println(">>>>  ¡¡¡ "+x +" es un número de Armstrong !!!  <<<<");
		else 
			System.out.println(">>>>  ¡¡¡ "+x +" NO es un número de Armstrong !!!  <<<<");
		System.out.println("------------------------------------");
		System.out.println(">>>>         MÉTODO RÁPIDO      <<<<");
		System.out.println("------------------------------------");
		if (Integers.armstrong(x))
			System.out.println(">>>>  ¡¡¡ "+x +" es un número de Armstrong !!!  <<<<");
		else 
			System.out.println(">>>>  ¡¡¡ "+x +" NO es un número de Armstrong !!!  <<<<");
		scanner.close();

		for(int i=0;i<10000;i++)
			if (Integers.armstrong(i)) 
				System.out.println(i);
	}

}
