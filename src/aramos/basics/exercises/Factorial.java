package aramos.basics.exercises;

import java.util.Scanner;

public class Factorial {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int x ;
		Scanner scanner = new Scanner(System.in);
		scanner.useDelimiter(System.getProperty("line.separator"));
		System.out.println("Escribe un número y calcularé su factorial... (no te pases del "+ Integer.MAX_VALUE +")");
		x= scanner.nextInt();
		System.out.println("Mmm... ok... me pongo con ello...");
		System.out.println("------------------------------------");
		System.out.println(">>>>       MÉTODO ITERATIVO     <<<<");
		System.out.println("------------------------------------");
		int resultado=factorialIter(x);
		System.out.println(resultado);
		
		
		System.out.println("------------------------------------");
		System.out.println(">>>>       MÉTODO RECURSIVO     <<<<");
		System.out.println("------------------------------------");
		resultado =factorialRecur(x);
		System.out.println(resultado);
		scanner.close();
	}

	private static int factorialRecur(int x) {
		if (x==1) return 1;
		return x*factorialRecur(x-1);
	}

	private static int factorialIter(int x) {
		int fact=1;
		for(int i=2;i<=x;i++){
			fact = fact*i;
		}
		return fact;
	}

}
