package aramos.basics.exercises;

import java.util.ArrayList;

public class Integers {

	public static long testprimo(long x2) {
		double sqrt_x2 = Math.sqrt(x2);
		for(int d=2;d<sqrt_x2; d++){
			if(x2%d==0) return d;
		}
		return 0;
	}

	public static boolean palindromeNum(long x){
		ArrayList<Short> cifras = getCifrasNumerico(x);
		int l = cifras.size();
		for(int i=0;i<l/2;i++){
			if(cifras.get(i).shortValue()!=cifras.get(l-1-i).shortValue()) return false;
		}
		return true;
	}
	
	public static boolean palindromeString(int x){
		String directo=String.valueOf(x);
		String reverso= new StringBuilder(directo).reverse().toString();
		return (directo.equals(reverso));

	}
	
	public static ArrayList<Short> getCifrasString(long x){
		ArrayList<Short> cifras = new ArrayList<Short>();
		String x_str=String.valueOf(x);
		String reverso= new StringBuilder(x_str).reverse().toString();
		int l=reverso.length();
		for(int index=0;index<l-1; index++){
			cifras.add(new Short(reverso.substring(index, index+1)));
		}
		return cifras;
	}
	
	public static ArrayList<Short> getCifrasNumerico(long x){
		ArrayList<Short> cifras = new ArrayList<Short>();
		while (x!=0){
			cifras.add(new Short((short) (x%10)));
			x=x/10;
		}
		return cifras;
	}
	public static int getNumeroDeCifras(long x){
		
		int i=0;
		while (x!=0){
			x=x/10;
			i++;
		}
		return i;
	}
	public static boolean potenciaDe2(long x){
		int p= (int) (Math.log(x)/Math.log(2));
		return (Math.pow(2, p)==x);
	}

	public static boolean potenciaDe2Bit(long x) {
		boolean flag=false;
		if (x<0) return false;
		for(int i=0;i<64-1;i++){
			if ((x & 0x0000000000000001)==0x0000000000000001) {
				if (flag) return false;
				flag=true;
			}
			x= (x >>>1);
		}
		return flag;
	}

	public static boolean armstrongJer(long x) {
		ArrayList<Short> cifras = getCifrasNumerico(x);
		long x2=0;
		for(int i=0; i<cifras.size(); i++){
			x2 += cifras.get(i)*cifras.get(i)*cifras.get(i);
		}
		return (x==x2);
	}

	public static boolean armstrong(long x) {
		double result = 0;
		long orig = x;
		int n=getNumeroDeCifras(x);
		while(x != 0){
			int remainder = (int) (x%10);
			result = result +  Math.pow(remainder,n);
			x = x/10;
		}
		if(orig == result){
			return true;
		}
		return false;
	}
}
