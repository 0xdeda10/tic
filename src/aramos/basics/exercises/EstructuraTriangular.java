package aramos.basics.exercises;

import java.util.Scanner;

public class EstructuraTriangular {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int x ;
		Scanner scanner = new Scanner(System.in);
		scanner.useDelimiter(System.getProperty("line.separator"));
		System.out.println("Escribe un número impar y haré una estructura triangular... (no te pases del "+ Integer.MAX_VALUE +")");
		x= scanner.nextInt();
		System.out.println("Mmm... ok... me pongo con ello...");
		String line="";
		for(int i=1;i<=x;i++){
			line=line+"*";
			if(i%2==0) continue;
			else System.out.println(line);
		}
		for(int i=1;i<=x;i++){
			line=line.substring(1);
			if(i%2!=0) continue;
			else System.out.println(line);
		}
		//Otra forma
		line="*";
		System.out.println();
		System.out.println("---------------------------");
		System.out.println();
		for(int i=1;i<=x/2;i++){
			System.out.println(line);
			line=line+"**";
		}
		
		for(int i=1;i<=x/2;i++){
			System.out.println(line);
			line=line.substring(2);
		}
		System.out.println(line);
		
		scanner.close();
	}

}
