package aramos.basics.exercises;

import java.util.Scanner;

public class PotenciaDe2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		long x ;
		Scanner scanner = new Scanner(System.in);
		scanner.useDelimiter(System.getProperty("line.separator"));
		System.out.println("Escribe un número y te diré si es potencia de 2... (no te pases del "+ Long.MAX_VALUE +")");
		x= scanner.nextLong();
		System.out.println("Mmm... ok... me pongo con ello...");
		System.out.println("------------------------------------");
		System.out.println(">>>>       MÉTODO LOGARÍTMICO   <<<<");
		System.out.println("------------------------------------");
		if (Integers.potenciaDe2(x))
			System.out.println(">>>>  ¡¡¡ "+x +" es potencia de 2 !!!  <<<<");
		else 
			System.out.println(">>>>  ¡¡¡ "+x +" NO es potencia de 2 !!!  <<<<");
		System.out.println("------------------------------------");
		System.out.println(">>>>  MÉTODO BASADO EN BITS     <<<<");
		System.out.println("------------------------------------");
		if (Integers.potenciaDe2Bit(x))
			System.out.println(">>>>  ¡¡¡ "+x +" es potencia de 2 !!!  <<<<");
		else 
			System.out.println(">>>>  ¡¡¡ "+x +" NO es potencia de 2 !!!  <<<<");
		scanner.close();
	}

}
