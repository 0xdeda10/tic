package aramos.crypto;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * 
 * @author alvaro
 * 
 */
public class SubCipherDecoder {

	private static final String DEFAULT_CORPUS = "corpus.txt";
	private static final char TOKEN = '|';

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		File corpus;
		String ciphermsg;

		corpus = new File(DEFAULT_CORPUS);
		Scanner scan = new Scanner(System.in);
		scan.useDelimiter(System.getProperty("line.separator"));
		ciphermsg = scan.next();

		SubCipherDecoder decoder = new SubCipherDecoder();
		decoder.train(corpus);
		decoder.decode(ciphermsg);

	}

	private Map<Character,Map<Character,Integer>> histogram;

	/**
	 * @param ciphermsg
	 */
	public void decode(String ciphermsg) {
		// TODO Auto-generated method stub

	}

	/**
	 * @param file
	 */
	public void train(File file) {
		
		histogram = new HashMap<Character,Map<Character,Integer>>();
		
		char last = TOKEN;
		char current= TOKEN;
		BufferedReader reader;
		boolean alphanumeric_last = false;
		try {
			reader = new BufferedReader(new FileReader(file));
			while (reader.ready()) {
				last = current;
				current = (char) reader.read();
				
				if (alphanumeric_last && alphanumeric(current)){
					Map<Character,Integer> ocurrence = getOrCreate(last);
					ocurrence.get(current);
				}
				
			}
			reader.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 

	}

	/**
	 * @param last
	 * @return
	 */
	private Map<Character, Integer> getOrCreate(Character last) {
		histogram.containsKey(last);
		return null;
		
	}

	/**
	 * @param current
	 * @return
	 */
	private boolean alphanumeric(char current) {
		// TODO Auto-generated method stub
		return false;
	}

}
