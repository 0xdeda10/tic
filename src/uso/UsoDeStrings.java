package uso;

public class UsoDeStrings {
	// Declarar una constante
	public static final String UNSTRINGCONSTANTE = "Soy un tio guay.";

	public static void main(String[] args) {
		String miString = UNSTRINGCONSTANTE;

		// Comparar Strings
		String otroString = new String("Soy un tio guay.");
		
		if (miString == otroString) {
			System.out.println("Son la misma instancia");
		} else {
			System.out.println("No son la misma instancia");
		}
		
		if (miString.equals(otroString)) {
			System.out.println("Tienen el mismo contenido");
		} else {
			System.out.println("No tienen el mismo contenido");
		}

		// Buscar en un string
		if (miString.contains("tio"))
			System.out.println("Palabra encontrada: 'tio'");
		if (!miString.contains("guayón")) {
			System.out.println("Palabra no encontrada: 'guayón");
		}

		// Buscar y reemplazar en una variable String
		String modificado = miString.replace("Soy", "El profesor de TIC es");

		System.out.println(modificado);

		String unString = "Hola, esto es un String.";
		String resultado;
		
		/*
		 * El método contains devuelve true si la cadena contiene la cadena que
		 * se pasa como parámetro.
		 * 
		 */
		
		if (unString.startsWith("Hola, ")) {
			/* 
			 * Si comienza por "Hola, "
			 * guardamos en resultado la cadena unString sin los primeros 6 caracteres
			 **/
			resultado = unString.substring(6);
			
			/* resultado contendrá “esto es un String.” */
			System.out.println(resultado);
			
		} else {
			System.out.println("unString no comienza con 'Hola, '");
		}
		
		/* resultado contendrá “esto es un String.” */
		
		int longitud = unString.length();
		/* longitud contendrá 24 */
		System.out.println("longitud = "+longitud);
		
		char c = unString.charAt(3);
		/* c contendrá 'a' */
		System.out.println("c = " + c);
		
	}

}
