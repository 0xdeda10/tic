package uso.colecciones;

import java.util.ArrayList;
import java.util.Scanner;

public class UsoDeArrayList {

	public static void main(String[] args) {
		
		/* Vamos a ver como podemos leer un conjunto de valores separados por comas
		 * usando dos Scanner
		 * 
		 * 
		 * Primero leemos la línea como siempre
		 */
		System.out.println("-- Uso de ArrayList --");
		System.out.println("Escribe una secuencia de enteros separados por comas:");
		Scanner sc = new Scanner(System.in);
		sc.useDelimiter(System.getProperty("line.separator"));
		String linea = sc.next();
		// Ahora linea tiene valores enteros separados por comas
		
		// Vamos a crear un Scanner sobre la linea leida pero que separe por comas
		Scanner scanner_comas = new Scanner(linea);
		scanner_comas.useDelimiter(",");
		
		
		// Vamos a crear un ArrayList de enteros
		ArrayList<Integer> un_arraylist = new ArrayList<Integer>();
				
		// Vamos a rellenar un ArrayList de enteros preguntando al Scanner si tiene más
		// fragmentos y mientras los tenga, los vamos añadiendo al ArrayList.
		while(scanner_comas.hasNextInt()){
			// Tomar el siguiente entero del Scanner
			int siguienteentero = scanner_comas.nextInt();
			// Añadir un elemento al ArrayList
			un_arraylist.add(siguienteentero);
		}
		System.out.println("-- Contenido del ArrayList --");
		System.out.println(un_arraylist);
		
		// Vamos a ver si está vacío
		boolean vacio = un_arraylist.isEmpty() ;
		System.out.println("¿Está vacío?: "+ vacio);
		// si está vacío salimos de main
		if (vacio) return;
		
		
		//Añadir un elemento en una posición determinada
		System.out.println("-- Añadimos un 4 en la posición 3 ArrayList --");
		un_arraylist.add(3,4);
		System.out.println(un_arraylist);
		
		//Ver si un elemento está en el ArrayList
		if (un_arraylist.contains(5))
			System.out.println("El ArrayList contiene el número 5");
		else System.out.println("El ArrayList no contiene el número 5");
		
		//Ver la posición de un elemento en el ArrayList
		int posicion = un_arraylist.indexOf(5);
		System.out.println("Primera posición del elemento '5' (si lo hay): "+ posicion);
				
		// vamos a recuperar un elemento del ArrayList (de la posición 2)
		System.out.println("Elemento en la posición 2: "+ un_arraylist.get(2));
		
		// vamos a ver su tamaño
		System.out.println("Tamaño del ArrayList: "+ un_arraylist.size() );
		
		// Vamos a ver si está vacío
		System.out.println("¿Está vacío?: "+ un_arraylist.isEmpty() );
		
		// Vamos a sustituir un elemento
		System.out.println("-- Sustituimos el elemento de la posición 2 del ArrayList por -4456 --");
		un_arraylist.set(2, -4456);
		System.out.println(un_arraylist);
		
		// vamos a extraer el elemento de la segunda posición
		System.out.println("-- Quitamos el elemento de la posición 1 del ArrayList --");
		un_arraylist.remove(1);
		System.out.println(un_arraylist);
		
		// vamos a extraer la primera ocurrencia del elemento 5 
		System.out.println("-- Quitamos la primera ocurrencia del elemento 5 (si lo hay) --");
		un_arraylist.remove(new Integer(5));
		System.out.println(un_arraylist);
		
		// vamos a convertirlo en un array
		Integer[] array = un_arraylist.toArray(new Integer[un_arraylist.size()]);
		System.out.println("Elemento 2 del array ( array[2] ): " + array[2]);
		
	}

}
