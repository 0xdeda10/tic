package uso.colecciones;

import java.util.Arrays;

public class UsoDeArrays {
	public static void main(String[] args) {
		
		//Creamos un array de Strings con 7 elementos
		String[] arrayDeStrings = new String[7];
		// Introducimos estos elementos
		arrayDeStrings[0] = "All";
		arrayDeStrings[1] = "your";
		arrayDeStrings[2] = "base";
		arrayDeStrings[3] = "are";
		arrayDeStrings[4] = "belong";
		arrayDeStrings[5] = "to";
		arrayDeStrings[6] = "us!";
		
		//Si imprimimos directamente el Array nos sale el identificador de objeto
		System.out.println("-- Ejecutando: System.out.println(arrayDeStrings) tenemos el id de objeto --");
		System.out.println(arrayDeStrings);
		
		//Para imprimirlo de forma que se vean sus elementos
		System.out.println();
		System.out.println("-- Para imprimirlo de forma que se vean sus elementos usamos Arrays.toString(arrayDeStrings) --");
		System.out.println(Arrays.toString(arrayDeStrings));
		
		// Vamos a recorrer el array y vamos a imprimirlo en una linea por 
		// pantalla con un espacio en blanco entre elementos
		System.out.println();
		System.out.println("-- Vamos a imprimirlo usando un bucle --");
		
		// guardamos en una variable la longitud del array
		int tamano =  arrayDeStrings.length;
		for(int k=0; k<tamano;k++){
			System.out.print(arrayDeStrings[k]+" ");
		}
		
		//Creamos un array de int con 10 elementos
		int[] arrayDeInts = new int[10];
		//Introducimos estos elementos
		arrayDeInts[0]=1;
		arrayDeInts[1]=345;
		arrayDeInts[2]=-3;
		arrayDeInts[3]=7;
		arrayDeInts[4]=43;
		arrayDeInts[5]=67;
		arrayDeInts[6]=8;
		arrayDeInts[7]=9;
		
		System.out.println();
		// ¿Qué pasa con las posiciones en las que no hemos introducido nada
		System.out.println();
		System.out.println("-- ¿Qué pasa con las posiciones 8 y 9?: toman el valor por defecto del tipo int (0) --");
		System.out.println(Arrays.toString(arrayDeInts));
		
	}
}
