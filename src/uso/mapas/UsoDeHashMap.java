package uso.mapas;

import java.util.Collection;
import java.util.HashMap;
import java.util.Scanner;

public class UsoDeHashMap {

	public static void main(String[] args) {

		System.out.println("-- Uso de HashMap --");

		// Creamos un HashMap y lo referenciamos con la variable notas
		HashMap<String, Integer> notas = new HashMap<String, Integer>();
		// Introducimos algunos datos en la forma clave-valor

		notas.put("TIC", 10);
		notas.put("Mates", 10);
		notas.put("Lengua", 8);
		notas.put("Física y química", 10);
		notas.put("Educación Física", 8);
		notas.put("Tecnología Industrial", 10);

		System.out.println();
		System.out.println("-- ¿está vacío el mapa? --");
		System.out.println(notas.isEmpty());
		
		
		/* Vamos a ver cuántas entradas hay en el mapa*/
		System.out.println();
		System.out.println("-- Hay " + notas.size() + " asignaturas con nota en el mapa");
		
		/* Imprimimos directamente el mapa */
		System.out.println();
		System.out.println("-- Imprimiendo directamente el mapa: System.out.println(notas) --");
		System.out.println(notas);
		
		/* Imprimimos los valores */
		System.out.println();
		System.out.println("-- Imprimiendo las claves --");
		Collection<String> claves = notas.keySet();
		System.out.println(claves);
		
		
		/* Imprimimos los valores */
		System.out.println();
		System.out.println("-- Imprimiendo los valores --");
		Collection<Integer> valores = notas.values();
		System.out.println(valores);
		
		
		System.out.println();
		/* Vamos a ver si contiene un valor concreto */
		if (notas.containsValue(10))
			System.out.println("-- Hay algun valor igual a 10");
	
		/* Le pedimos al usuario que introduzca el nombre de la asignatura */
		System.out.println();
		System.out.println("-- Introduce la asignatura: --");
		Scanner sc = new Scanner(System.in);
		sc.useDelimiter(System.getProperty("line.separator"));
		String asignatura = sc.next();
		
		/* Vamos a ver si contiene esta clave */
		System.out.println();
		System.out.println("-- Comprobando que existe la asignatura...");
		if (notas.containsKey(asignatura)) {
			System.out.println("... asignatura encontrada");
			System.out.println();
			/* Recuperamos del hashMap el valor correspondiente a esa clave */
			Integer nota = notas.get(asignatura);
			System.out.println("La nota de " + asignatura + " es " + nota);

		} else {
			
			// Si no existe la clave en el mapa HashMap
			System.out.println("No existe nota para esa asignatura");
		}

		/* Vamos a borrar el mapa */
		notas.clear();
	}

}
