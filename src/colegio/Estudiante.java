package colegio;

public class Estudiante extends Persona {
	
	//Variables miembro
	public String grupo;
	public String tutor;
	
	//Constructor
	public Estudiante(String nombre, String apellido1, String apellido2) {
		super(nombre, apellido1, apellido2);
	}
	
	//Métodos
	public String getGrupo() {
		return grupo;
	}
	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	public String getTutor() {
		return tutor;
	}
	public void setTutor(String tutor) {
		this.tutor = tutor;
	}
	public void estudia(Materia materia){
		System.out.println("El alumno "+ this.getNombre() + " "+ this.getApellido1() + " " + 
				this.getApellido2() + " estudia la materia " + materia.getNombre());
	}
}
