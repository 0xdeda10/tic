package colegio;

public class Profesor extends Persona {

	public Profesor(String nombre, String apellido1, String apellido2) {
		super(nombre, apellido1, apellido2);
	}
	
	public void imparte(Materia materia){
		System.out.println("El profesor "+ this.getNombre() + " "+ this.getApellido1() + " " + 
					this.getApellido2() + " imparte la materia " + materia.getNombre());
	}
}
