package colegio;

public class Materia {

	private String nombre;

	public Materia(String nombre) {
		super();
		this.nombre = nombre;
	}

	public String getNombre() {
		return nombre;
	}
	
}
