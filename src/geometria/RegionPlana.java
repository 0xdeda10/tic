package geometria;

// Una clase abstracta (con modificador "abstract") permite 
// definir métodos sin implementarlos.
// Esto obliga a las clases que heredan a implementarlos.

public abstract class RegionPlana {

	/* *********************************
	 * 			Métodos
	 ***********************************/
	
	public abstract double perimetro();
	public abstract double area();
	public abstract boolean contieneElPunto(Punto p);
	
	public int getNumDimensiones(){
		return 2;
	}
}
