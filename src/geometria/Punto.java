package geometria;

public class Punto {
	/* *********************************
	 * 			Variables Miembro
	 ***********************************/
	
	double x,y;
	

	/* *********************************
	 * 			Constructores
	 ***********************************/
	
	// los comentarios que comienzan por /** y terminan por **/
	// se llaman javadoc y sirven para documentar el código 
	// Eclipse y otros IDE incluyen los comentarios javadoc dentro de la ayuda
	
	/**
	 * 
	 * Constructor basado en coordenadas
	 * 
	 * @param x abscisa 
	 * @param y ordenada
	 *
	 **/
	public Punto(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}
	/* *********************************
	 * 			Métodos
	 ***********************************/
	public int cuadrante() {
		// Si no está en ningún cuadrante devolvemos -1
		if ((x==0) && (y==0)) return -1;
		if (x>0){
			if (y>0)
				return 1;
			else 
				return 4;
		} else {
			if (y>0)
				return 2;
			else 
				return 3;
		}
	}
	
	public double distanciaA(Punto p1){
		return distancia(p1,this);
	}
	
	// Un método de clase (static) se puede invocar sin que haya ninguna instancia
	public static double distancia(Punto p1,Punto p2){
		return Math.sqrt(Math.pow(p2.x - p1.x,2) + Math.pow(p2.y - p1.y,2));
	}
	
}
