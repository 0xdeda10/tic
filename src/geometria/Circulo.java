package geometria;

public class Circulo extends RegionPlana {
	
	/* *********************************
	 * 			Constantes
	 ***********************************/
	
	// Las constantes en Java se declaran con la palabra "final" 
	//y se suelen declarar con el nombre en mayúsculas 
	public static final double PI = 3.1416;
	
	
	/* *********************************
	 * 			Variables Miembro
	 ***********************************/
	
	//las variables miembro describen la información estática del objeto.
	private double radio;
	private Punto centro;
	
	
	/* *********************************
	 * 			Constructores
	 ***********************************/
	
	//El constructor indica como se crea una instancia del objeto.
	public Circulo(Punto centro, double radio) {
		super();
		this.radio = radio;
		this.centro = centro;
	}
	
	
	/* *********************************
	 * 			Métodos
	 ***********************************/
	
	//Los métodos permiten realizar operaciones internas del objeto y posiblemente, devolver un resultado
	
	/* La anotación @Override indica que estamos implementando un método declarado en la superclase (RegionPlana)
	* Esta anotación es opcional.
	*/
	@Override
	public double perimetro(){
		return 2*PI*radio;
	}
	@Override
	public double area(){
		return PI*radio*radio;
	}
	
	@Override
	public boolean contieneElPunto(Punto p) {
		return ( p.distanciaA(centro)<= radio );
	}
	
	// Este método nos permite probar el funcionamiento de la clase Circulo
	
	public static void main(String[] args) {
		Circulo c = new Circulo( new Punto(2,3) , 1);
		
		System.out.println("Perimetro: "+ c.perimetro());
		System.out.println("Área: "+ c.area());
		
		
		Punto p1 = new Punto(1,-2);
		System.out.println("Pertenece (1,-2) al Círculo?: "+ c.contieneElPunto(p1));
		
		// Las instancias de Circulo heredan los métodos de la superclase (RegionPlana)
		System.out.println("Dimensiones de la región: "+c.getNumDimensiones());
		
		// Podemos tratar Circulo como si fuera un objeto de la superclase (RegionPlana)
		RegionPlana r1 = new Circulo( new Punto(0, -1) , 100);
		System.out.println("Perimetro de la region plana r1 : "+ r1.perimetro());
	}	
}
