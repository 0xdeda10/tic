package geometria;

public class Recta {
	
	/* *********************************
	 * 			Variables Miembro
	 ***********************************/
	
	// coeficientes de la ecuación general
	// a x + b y + c = 0;
	
	double a,b,c;
	
	
	/* *********************************
	 * 			Constructores
	 ***********************************/
	
	/**
	 * 
	 * Constructor basado en dos puntos
	 * 
	 * @param p1 primer punto
	 * @param p2 segundo punto
	 * 
	 **/
	public Recta(Punto p1,Punto p2) {
		super();
		a= p2.y-p1.y; //d_y
		b= p1.x-p2.x;  // -d_x
		c= -a*p1.x - b*p1.y; 
	}
	
	public Recta(double a, double b, double c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
	}


	/* *********************************
	 * 			Métodos
	 ***********************************/
	
	/**
	 * Devuelve True si ambos puntos se encuentran en el mismo semiplano definido por esta recta
	 * 
	 * @param p1 primer punto
	 * @param p2 segundo punto
	 * @return  True si ambos puntos se encuentran en el mismo semiplano
	 *  **/
	public boolean estanEnMismoSemiplano(Punto p1, Punto p2){
		
		// Ejemplo de expresión booleana compleja... se puede usar Math.signum()
		if(((a*p1.x+b*p1.y + c>0)&&(a*p2.x+b*p2.y + c>0)) || ((a*p1.x+b*p1.y + c<0)&&(a*p2.x+b*p2.y + c<0))) return true;
		else return false; 
	}
	
	/**
	 * Devuelve la distancia de un punto a esta recta
	 * 
	 * @param x abscisa del punto
	 * @param y abscisa del punto
	 * @return  la distancia a esta recta
	 *  
	 **/
	public double distancia(Punto p){
		return Math.abs(a*p.x+b*p.y + c)/Math.sqrt(Math.pow(a,2) + Math.pow(b,2));
	}
}
